# Fortress Python Skeleton (v1.0.0)

## Background

The Fortress Python Skeleton is a nearly empty project designed to get you started on developing a Python service for Fortress Information Security Central Services. It includes just a few things to guide you in the direction that we have found is useful. This relatively uniform start is also intended to aid in maintenance and continued development of projects by teams that familiarize themselves with this sort of structure.

This project was born out of discussions in early 2022 between Ross Miller, Hayden Salmon, and Jake Oliger, under the direction of Daniel Marshall and Ben Smith. A need was identified to professionalize and standardize our services to improve future productivity and make design decisions easier. This began with rewrites of Overseer (Cyber Hygiene Scanner) and Data Driven Risk Rank alongside the maturing development of Pantograph.

## Setup

Prerequisites:

- Docker

Once you have cloned the repository and installed Docker, begin by ensuring the project builds properly.
<!-- TODO: add description surrounding importing project variables after discussion regarding config.ini vs .env files -->

```
docker-compose up --build
```

(For more information on `docker-compose`, please consult the [Docker docs](https://docs.docker.com/compose/reference/up/).)

It is recommended to rename the `src` directory to a Python-package-friendly name (like `ddrr` or `overseer`) to ease absolute module path references within your code. Doing so will also require updating three references to that path: in `docker-compose.yml` on lines 42 and 58, and in `src/Dockerfile` on line 5. (These line numbers are subject to change -- look around or Ctrl-F if they don't match up. If you notice they don't match up... fix this README, too!)

Additionally, it is recommended to update `requirements.txt` to specify versions for the Python packages you are using at the time of development to prevent ambiguity down the road.

For specifics regarding individual folder specifications please reference the README.MD file in the respecive folder.
