from celery.result import AsyncResult

from __init__ import app, log
from utils.api_structs import Groups
from worker.tasks import run_task



@app.post(
    "/start/task",
    description="This is a description",
    summary="this is a summary",
)
async def start_task(content: Groups):

    log.info("this is a test log")
    
    task = run_task.delay(content.groups)
    return task.id


@app.get(
    "/status/{task_id}",
    description="This is a description",
    summary="this is a summary",
)
def status(task_id):

    result = AsyncResult(task_id)
    return {
        "task_id": task_id, 
        "status": result.status
    }
