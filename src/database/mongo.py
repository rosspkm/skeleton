import pymongo

from __init__ import log, config


# mongo connection client
class MongoConnect():

    def __init__(self):
        self.client = self.__connect()
        self.database = self.__database(config.database_name)
    
    def __connect(self):
        try:
            return pymongo.MongoClient(host=config.mongo_uri)
        except:
            log.critical("Unable to establish a connection to MongoDB: %s", config.mongo_uri, exc_info=1)
            raise ConnectionRefusedError

    def __database(self, database: str):
        try:
            return self.client[database]
        except:
            log.critical("Unable to connect to Mongo Database: %s", database, exc_info=1)
            raise ConnectionRefusedError

    def insert(self, collection: str, content: dict):
        if not collection:
            log.error("No collection was supplied", exc_info=1)

            return False
        
        if not content:
            log.error("No content was given to upload into mongo", exc_info=1)

            return False

        try:
            col = self.database[collection]
            col.insert_one(content)
            log.info("content: %s was submitted to collection: %s", content, collection)

            return True

        except:
            log.error("Unable to insert %s into collection: %s", content, col, exc_info=1)

            return False

    def find(self, collection: str, content: dict, exclude: dict = None):
        if not collection:
            log.error("No collection was supplied", exc_info=1)

            return False

        if not content:
            log.error("No content was given to upload into mongo", exc_info=1)

            return False

        try:
            col = self.database[collection]
            if not exclude:
                return col.find_one(content)
            else:
                col.find_one(content, exclude)

        except:
            log.error("No content was found for: %s", content, exc_info=1)
            return False

    def update(self, collection: str, query: dict, content: dict):
        if not collection:
            log.error("No collection was supplied", exc_info=1)

            return False
        
        if not content:
            log.error("No content was given to upload into mongo", exc_info=1)

            return False

        try:
            col = self.database[collection]
            query = query
            new_values = {"$set": content}
            col.update_one(query, new_values)
            log.info("content: %s was submitted updated to query: %s in collection: %s", content, query, collection)

            return True
        except:
            log.error("Unable to update the values on the query %s for collection %s to %s", query, col, new_values, exc_info=1)

            return False
    
    def close(self):
        self.client.close()

def update_document(collection: str, query: dict, content: dict):
    if config.use_mongo:
        mongo = MongoConnect()
        updated = mongo.update(collection=collection, query=query, content=content)
        mongo.close()
        if updated:
            pass
        else:
            return None

    return True

def insert_document(collection: str, content: dict):
    if config.use_mongo:
        mongo = MongoConnect()
        inserted = mongo.insert(collection=collection, content=content)
        mongo.close()

        if inserted:
            pass
        else:
            return None

    return True

def find_document(collection: str, content: dict):
    if config.use_mongo:
        mongo = MongoConnect()
        found = mongo.find(collection=collection, content=content)
        mongo.close()

        if found:
            return found
        else:
            return None

    return None

def get_scan(scan_id: str):
    if config.use_mongo:
        mongo = MongoConnect()
        scan = mongo.find_scan(scan_id=scan_id)
        mongo.close()

        if not scan:
            return f"Could not find a scan with the scan_id: {scan_id}"

        return scan

    return "Database connection set to false"

def get_status(scan_id: str):
    if config.use_mongo:
        mongo = MongoConnect()
        scan = mongo.find_scan(scan_id=scan_id)
        mongo.close()

        if not scan:
            return f"Could not find a scan with the scan_id: {scan_id}"

        del scan["results"]
        return scan

    return "Database connection set to false"