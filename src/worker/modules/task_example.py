import json

from __init__ import project_path

def task_example(content):

    with open(f'{project_path}/worker/resources/example_resource.json') as file:
        data = json.load(file)

    print(data)
    for i in range(0, 10):
        print(i)

    return content
