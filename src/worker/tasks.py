import time

from __init__ import celery
from worker.modules.task_example import task_example

@celery.task(name="run_task")
def run_task(content):
    time.sleep(1)
    return task_example(content)
