import os

from fastapi import FastAPI
from celery import Celery

from utils.config import Config

project_path = os.path.dirname(os.path.abspath(__file__))

config = Config()

# connect to celery
celery = Celery(__name__)
celery.conf.update(
    broker_url=config.celery_broker,
    result_backend=config.celery_backend,
    task_acks_late=True,
    broker_heartbeat=0,
    broker_pool_limit=0,
    broker_heartbeat_checkrate=2.0,
    broker_transport_options={"confirm_publish": True},
    broker_connection_retry=True,
    broker_connection_max_retries=100,
    celery_task_track_started=True,
    celery_worker_concurrency=4,
    celery_max_tasks_per_child=8,
)
# DO NOT REMOVE
# prevents future celery imports from interacting with non-configured instances
celery.set_default()

# set up loging for app
log = config.get_logger("app-name")

log.info("starting app")

app = FastAPI()
