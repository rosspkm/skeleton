import configparser
import logging
import os

from utils.log_handlers import ElasticHandler

class Config():
    def __init__(self):
        # Grab the deployment mode env variable
        MODE = os.getenv('DEPLOYMENT_MODE')
        if not MODE:
            raise Exception('No deployment mode set, please set DEPLOYMENT_MODE enviornment variable')
        # Pull the config information from the file
        config_file_path = os.getenv('CONFIG_PATH')
        if not config_file_path:
            raise Exception('No path to config file set, please set CONFIG_PATH enviornment variable')
        c = configparser.ConfigParser()
        c.read(config_file_path)
        if c.read(config_file_path) == []:
            raise Exception('Supplied config file {0} was not found'.format(config_file_path))
        # Load in variables from config file, based on the mode defined
        try: 
            # NOTE - If using docker, the backend and broker connection string should use the redis/rabbitmq container names instead of localhost
            self.celery_backend = c.get(MODE, 'CELERY_BACKEND_CONN_STR')
            self.celery_broker = c.get(MODE, 'CELERY_BROKER_CONN_STR')
            # Some configuration on logging
            self.elasticsearch_log_index = c.get(MODE, 'ELASTICSEARCH_LOG_INDEX')
            self.elasticsearch_connection_string = c.get(MODE, 'ELASTICSEARCH_CONN_STR')
            self.log_level = c.get(MODE, 'LOG_LEVEL')
            self.instance_name = c.get(MODE, 'INSTANCE_NAME')
            self.use_es = c.getboolean(MODE, 'USE_ES')
        except Exception as e: 
            raise Exception('Failed to load in defined parameters from config file {0}'.format(config_file_path))

    def get_logger(self, logger_name):
        logger = logging.getLogger(logger_name)
        logger.setLevel(self.log_level)

        # if true use elastic search logger also
        if self.use_es:
            eh = ElasticHandler(self.instance_name, self.elasticsearch_log_index, self.elasticsearch_connection_string, self.log_level)
            logger.addHandler(eh)

        return logger