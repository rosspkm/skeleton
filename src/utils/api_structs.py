from pydantic import BaseModel
from typing import List

# https://fastapi.tiangolo.com/tutorial/response-model/?h=response
class Groups(BaseModel):
    groups: List[str]
