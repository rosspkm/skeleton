from datetime import datetime
from logging import StreamHandler
from elasticsearch import Elasticsearch
from datetime import datetime
import logging
import json

# DEBUG:      Detailed information, typically of interest only when diagnosing problems.
# INFO:       Confirmation that things are working as expected.
# WARNING:    An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.
# ERROR:      Due to a more serious problem, the software has not been able to perform some function.
# CRITICAL:   A serious error, indicating that the program itself may be unable to continue running.
class ElasticHandler(StreamHandler):
    def __init__(self, instance_name, elasticsearch_log_index, elasticsearch_connection_string, log_level):
        StreamHandler.__init__(self)
        self.instance_name = str(instance_name)
        self.elasticsearch_log_index = str(elasticsearch_log_index)
        # Configure logger
        formatter = logging.Formatter('[%(asctime)s : %(levelname)s] -  %(message)s\n')
        self.setLevel(log_level)
        self.setFormatter(formatter)
        # Configure elasticsearch and check if connection is open
        self.es = None
        self.es = Elasticsearch(str(elasticsearch_connection_string))
        if not self.es.ping():
            raise ConnectionRefusedError('ElasticHandler could not connect to Elasticsearch, please check network or config settings')

    def send_to_elasticsearch(self, log_doc):
        try:
            self.es.index(index=f"{self.elasticsearch_log_index}-{str(datetime.today().strftime('%Y.%m.%d'))}", document=log_doc)
        except Exception as exc:
            print(exc)

    def emit(self, log_info):
        meta = dict(log_info.meta) if "meta" in list(log_info.__dict__.keys()) else {}        
        meta = json.dumps(meta)
        meta = json.loads(meta)
        # Create a dict for all the data to be emitted to elasticsearch
        log_doc = {
            "app_instance" : self.instance_name,
            "message" : log_info.msg,
            "timestamp" : datetime.utcnow(),
            "context" : {
                "level" : log_info.levelname,
                "file_name" : log_info.filename,
                "func_name" : log_info.funcName,
                "path_name" : log_info.pathname,
                "trace" : str(log_info.exc_info),
                "line_number" : log_info.lineno
            },
            "meta_data" : meta
        }
        # Sebd the data over to elasticsearch in background
        self.send_to_elasticsearch(log_doc)
        # Emit the formatted log to the output steam (default is console)
        stream = self.stream
        record = str(self.formatter.format(log_info))
        stream.write(record)